// SPDX-License-Identifier: LicenseRef-Go
// SPDX-FileCopyrightText: The Go Authors.

// Copyright 2022 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package lcs

import (
	"fmt"
	"strings"
)

const msgBrokenPath = "broken path"

// Non-generic code. The names have Old at the end to indicate they are
// the implementation that doesn't use generics.

// Compute the Diffs and the lcs.
func Compute(a, b interface{}, limit int) ([]Diff, lcs) {
	var ans lcs

	g := newEditGraph(a, b, limit)
	ans = g.twoSided()
	diffs := g.fromLcs(ans)

	return diffs, ans
}

// editGraph carries the information for computing the lcs for []byte, []rune, or []string.
type editGraph struct {
	eq     eq    // how to compare elements of A, B, and convert slices to strings
	vf, vb label // forward and backward labels

	limit int // maximal value of D
	// the bounding rectangle of the current edit graph
	lx, ly, ux, uy int
	delta          int // common subexpression: (ux-lx)-(uy-ly)
}

// abstraction in place of generic
type eq interface {
	eq(i, j int) bool
	substr(i, j int) string // string from b[i:j]
	lenA() int
	lenB() int
}

type byteEq struct {
	a, b []byte // the input was ascii. perhaps these could be strings
}

func (x *byteEq) eq(i, j int) bool       { return x.a[i] == x.b[j] }
func (x *byteEq) substr(i, j int) string { return string(x.b[i:j]) }
func (x *byteEq) lenA() int              { return len(x.a) }
func (x *byteEq) lenB() int              { return len(x.b) }

type runeEq struct {
	a, b []rune
}

func (x *runeEq) eq(i, j int) bool       { return x.a[i] == x.b[j] }
func (x *runeEq) substr(i, j int) string { return string(x.b[i:j]) }
func (x *runeEq) lenA() int              { return len(x.a) }
func (x *runeEq) lenB() int              { return len(x.b) }

type lineEq struct {
	a, b []string
}

func (x *lineEq) eq(i, j int) bool       { return x.a[i] == x.b[j] }
func (x *lineEq) substr(i, j int) string { return strings.Join(x.b[i:j], "") }
func (x *lineEq) lenA() int              { return len(x.a) }
func (x *lineEq) lenB() int              { return len(x.b) }

func newEq(a, b interface{}) eq {
	switch x := a.(type) {
	case []byte:
		return &byteEq{a: x, b: b.([]byte)}
	case []rune:
		return &runeEq{a: x, b: b.([]rune)}
	case []string:
		return &lineEq{a: x, b: b.([]string)}
	default:
		panic(fmt.Sprintf("unexpected type %T in newEq", x))
	}
}

func (e *editGraph) fromLcs(lcs lcs) []Diff {
	var ans []Diff

	var pa, pb int // offsets in a, b

	for _, l := range lcs {
		if pa < l.X && pb < l.Y {
			ans = append(ans, Diff{pa, l.X, e.eq.substr(pb, l.Y)})
		} else if pa < l.X {
			ans = append(ans, Diff{pa, l.X, ""})
		} else if pb < l.Y {
			ans = append(ans, Diff{pa, l.X, e.eq.substr(pb, l.Y)})
		}

		pa = l.X + l.Len
		pb = l.Y + l.Len
	}

	if pa < e.eq.lenA() && pb < e.eq.lenB() {
		ans = append(ans, Diff{pa, e.eq.lenA(), e.eq.substr(pb, e.eq.lenB())})
	} else if pa < e.eq.lenA() {
		ans = append(ans, Diff{pa, e.eq.lenA(), ""})
	} else if pb < e.eq.lenB() {
		ans = append(ans, Diff{pa, e.eq.lenA(), e.eq.substr(pb, e.eq.lenB())})
	}

	return ans
}

func newEditGraph(a, b interface{}, limit int) *editGraph {
	if limit <= 0 {
		limit = 1 << 25 // effectively infinity
	}

	var aLen, bLen int

	switch a := a.(type) {
	case []byte:
		aLen, bLen = len(a), len(b.([]byte))
	case []rune:
		aLen, bLen = len(a), len(b.([]rune))
	case []string:
		aLen, bLen = len(a), len(b.([]string))
	default:
		panic(fmt.Sprintf("unexpected type %T in newEditGraph", a))
	}

	ans := &editGraph{eq: newEq(a, b), vf: newtriang(limit), vb: newtriang(limit), limit: limit,
		ux: aLen, uy: bLen, delta: aLen - bLen}

	return ans
}

// --- FORWARD ---
// forwardDone decides if the forward path has reached the upper right
// corner of the rectangle. If so, it also returns the computed lcs.
func (e *editGraph) forwardDone(D, k int) (bool, lcs) {
	// x, y, k are relative to the rectangle
	x := e.vf.get(D, k)
	y := x - k

	if x == e.ux && y == e.uy {
		return true, e.forwardLcs(D, k)
	}

	return false, nil
}

// run the forward algorithm, until success or up to the limit on D.
func (e *editGraph) forward() lcs {
	e.setForward(0, 0, e.lx)

	if ok, ans := e.forwardDone(0, 0); ok {
		return ans
	}

	// from D to D+1
	for D := 0; D < e.limit; D++ {
		e.setForward(D+1, -(D + 1), e.getForward(D, -D))

		if ok, ans := e.forwardDone(D+1, -(D + 1)); ok {
			return ans
		}

		e.setForward(D+1, D+1, e.getForward(D, D)+1)

		if ok, ans := e.forwardDone(D+1, D+1); ok {
			return ans
		}

		for k := -D + 1; k <= D-1; k += 2 {
			// these are tricky and easy to get backwards
			lookV := e.lookForward(k, e.getForward(D, k-1)+1)
			lookH := e.lookForward(k, e.getForward(D, k+1))

			if lookV > lookH {
				e.setForward(D+1, k, lookV)
			} else {
				e.setForward(D+1, k, lookH)
			}

			if ok, ans := e.forwardDone(D+1, k); ok {
				return ans
			}
		}
	}
	// D is too large.
	// Find the D path with maximal x+y inside the rectangle and
	// use that to compute the found part of the lcs
	kMax := -e.limit - 1
	diagMax := -1

	for k := -e.limit; k <= e.limit; k += 2 {
		x := e.getForward(e.limit, k)
		y := x - k

		if x+y > diagMax && x <= e.ux && y <= e.uy {
			diagMax, kMax = x+y, k
		}
	}

	return e.forwardLcs(e.limit, kMax)
}

// recover the lcs by backtracking from the farthest point reached
func (e *editGraph) forwardLcs(D, k int) lcs {
	var ans lcs

	for x := e.getForward(D, k); x != 0 || x-k != 0; {
		if ok(D-1, k-1) && x-1 == e.getForward(D-1, k-1) {
			// if (x-1,y) is labelled D-1, x--,D--,k--,continue
			D, k, x = D-1, k-1, x-1
			continue
		} else if ok(D-1, k+1) && x == e.getForward(D-1, k+1) {
			// if (x,y-1) is labelled D-1, x, D--,k++, continue
			D, k = D-1, k+1
			continue
		}
		// if (x-1,y-1)--(x,y) is a diagonal, prepend,x--,y--, continue
		y := x - k
		realX, realY := x+e.lx, y+e.ly

		if e.eq.eq(realX-1, realY-1) {
			ans = prependLcs(ans, realX-1, realY-1)
			x--
		} else {
			panic(msgBrokenPath)
		}
	}

	return ans
}

// start at (x,y), go up the diagonal as far as possible,
// and label the result with d
func (e *editGraph) lookForward(k, relX int) int {
	rely := relX - k
	x, y := relX+e.lx, rely+e.ly

	for x < e.ux && y < e.uy && e.eq.eq(x, y) {
		x++
		y++
	}

	return x
}

func (e *editGraph) setForward(d, k, relX int) {
	x := e.lookForward(k, relX)
	e.vf.set(d, k, x-e.lx)
}

func (e *editGraph) getForward(d, k int) int {
	x := e.vf.get(d, k)
	return x
}

// --- BACKWARD ---
// backwardDone decides if the backward path has reached the lower left corner
func (e *editGraph) backwardDone(D, k int) (bool, lcs) {
	// x, y, k are relative to the rectangle
	x := e.vb.get(D, k)
	y := x - (k + e.delta)

	if x == 0 && y == 0 {
		return true, e.backwardLcs(D, k)
	}

	return false, nil
}

// run the backward algorithm, until success or up to the limit on D.
func (e *editGraph) backward() lcs {
	e.setBackward(0, 0, e.ux)

	if ok, ans := e.backwardDone(0, 0); ok {
		return ans
	}
	// from D to D+1
	for D := 0; D < e.limit; D++ {
		e.setBackward(D+1, -(D + 1), e.getBackward(D, -D)-1)

		if ok, ans := e.backwardDone(D+1, -(D + 1)); ok {
			return ans
		}

		e.setBackward(D+1, D+1, e.getBackward(D, D))

		if ok, ans := e.backwardDone(D+1, D+1); ok {
			return ans
		}

		for k := -D + 1; k <= D-1; k += 2 {
			// these are tricky and easy to get wrong
			lookV := e.lookBackward(k, e.getBackward(D, k-1))
			lookH := e.lookBackward(k, e.getBackward(D, k+1)-1)

			if lookV < lookH {
				e.setBackward(D+1, k, lookV)
			} else {
				e.setBackward(D+1, k, lookH)
			}

			if ok, ans := e.backwardDone(D+1, k); ok {
				return ans
			}
		}
	}

	// D is too large
	// find the D path with minimal x+y inside the rectangle and
	// use that to compute the part of the lcs found
	kMax := -e.limit - 1
	diagMin := 1 << 25

	for k := -e.limit; k <= e.limit; k += 2 {
		x := e.getBackward(e.limit, k)
		y := x - (k + e.delta)

		if x+y < diagMin && x >= 0 && y >= 0 {
			diagMin, kMax = x+y, k
		}
	}

	if kMax < -e.limit {
		panic(fmt.Sprintf("no paths when limit=%d?", e.limit))
	}

	return e.backwardLcs(e.limit, kMax)
}

// recover the lcs by backtracking
func (e *editGraph) backwardLcs(D, k int) lcs {
	var ans lcs

	for x := e.getBackward(D, k); x != e.ux || x-(k+e.delta) != e.uy; {
		if ok(D-1, k-1) && x == e.getBackward(D-1, k-1) {
			// D--, k--, x unchanged
			D, k = D-1, k-1
			continue
		} else if ok(D-1, k+1) && x+1 == e.getBackward(D-1, k+1) {
			// D--, k++, x++
			D, k, x = D-1, k+1, x+1
			continue
		}

		y := x - (k + e.delta)
		realX, realY := x+e.lx, y+e.ly

		if e.eq.eq(realX, realY) {
			ans = appendLcs(ans, realX, realY)
			x++
		} else {
			panic(msgBrokenPath)
		}
	}

	return ans
}

// start at (x,y), go down the diagonal as far as possible,
func (e *editGraph) lookBackward(k, relX int) int {
	rely := relX - (k + e.delta) // forward k = k + e.delta
	x, y := relX+e.lx, rely+e.ly

	for x > 0 && y > 0 && e.eq.eq(x-1, y-1) {
		x--
		y--
	}

	return x
}

// convert to rectangle and label the result with d
func (e *editGraph) setBackward(d, k, relX int) {
	x := e.lookBackward(k, relX)
	e.vb.set(d, k, x-e.lx)
}

func (e *editGraph) getBackward(d, k int) int {
	x := e.vb.get(d, k)
	return x
}

// -- TWO-SIDED ---

func (e *editGraph) twoSided() lcs {
	// The termination condition could be improved, as either the forward
	// or backward pass could succeed before Myers' Lemma applies.
	// Aside from questions of efficiency (is the extra testing cost-effective)
	// this is more likely to matter when e.limit is reached.
	e.setForward(0, 0, e.lx)
	e.setBackward(0, 0, e.ux)

	// from D to D+1
	for D := 0; D < e.limit; D++ {
		// just finished a backwards pass, so check
		if got, ok := e.twoDone(D, D); ok {
			return e.twoLcs(D, D, got)
		}
		// do a forwards pass (D to D+1)
		e.setForward(D+1, -(D + 1), e.getForward(D, -D))
		e.setForward(D+1, D+1, e.getForward(D, D)+1)

		for k := -D + 1; k <= D-1; k += 2 {
			// these are tricky and easy to get backwards
			lookV := e.lookForward(k, e.getForward(D, k-1)+1)
			lookH := e.lookForward(k, e.getForward(D, k+1))

			if lookV > lookH {
				e.setForward(D+1, k, lookV)
			} else {
				e.setForward(D+1, k, lookH)
			}
		}
		// just did a forward pass, so check
		if got, ok := e.twoDone(D+1, D); ok {
			return e.twoLcs(D+1, D, got)
		}
		// do a backward pass, D to D+1
		e.setBackward(D+1, -(D + 1), e.getBackward(D, -D)-1)
		e.setBackward(D+1, D+1, e.getBackward(D, D))

		for k := -D + 1; k <= D-1; k += 2 {
			// these are tricky and easy to get wrong
			lookV := e.lookBackward(k, e.getBackward(D, k-1))
			lookH := e.lookBackward(k, e.getBackward(D, k+1)-1)

			if lookV < lookH {
				e.setBackward(D+1, k, lookV)
			} else {
				e.setBackward(D+1, k, lookH)
			}
		}
	}

	// D too large.
	// Combine a forward and backward partial lcs first, a forward one
	kMax := -e.limit - 1
	diagMax := -1

	for k := -e.limit; k <= e.limit; k += 2 {
		x := e.getForward(e.limit, k)
		y := x - k

		if x+y > diagMax && x <= e.ux && y <= e.uy {
			diagMax, kMax = x+y, k
		}
	}

	if kMax < -e.limit {
		panic(fmt.Sprintf("no forward paths when limit=%d?", e.limit))
	}

	lcs := e.forwardLcs(e.limit, kMax)
	// Now a backward one.
	// Find the D path with minimal x+y inside the rectangle and
	// use that to compute the lcs
	diagMin := 1 << 25 // infinity

	for k := -e.limit; k <= e.limit; k += 2 {
		x := e.getBackward(e.limit, k)
		y := x - (k + e.delta)

		if x+y < diagMin && x >= 0 && y >= 0 {
			diagMin, kMax = x+y, k
		}
	}

	if kMax < -e.limit {
		panic(fmt.Sprintf("no backward paths when limit=%d?", e.limit))
	}

	lcs = append(lcs, e.backwardLcs(e.limit, kMax)...)
	// These may overlap (e.forwardLcs and e.backwardLcs return sorted lcs)
	ans := lcs.fix()

	return ans
}

// Does Myers' Lemma apply?
func (e *editGraph) twoDone(df, db int) (int, bool) {
	if (df+db+e.delta)%2 != 0 {
		return 0, false // diagonals cannot overlap
	}

	kMin := -db + e.delta
	if -df > kMin {
		kMin = -df
	}

	kMax := db + e.delta
	if df < kMax {
		kMax = df
	}

	for k := kMin; k <= kMax; k += 2 {
		x := e.vf.get(df, k)
		u := e.vb.get(db, k-e.delta)

		if u <= x {
			// is it worth looking at all the other k?
			for l := k; l <= kMax; l += 2 {
				x := e.vf.get(df, l)
				y := x - l
				u := e.vb.get(db, l-e.delta)
				v := u - l

				if x == u || u == 0 || v == 0 || y == e.uy || x == e.ux {
					return l, true
				}
			}

			return k, true
		}
	}

	return 0, false
}

func (e *editGraph) twoLcs(df, db, kf int) lcs {
	// db==df || db+1==df
	x := e.vf.get(df, kf)
	y := x - kf
	kb := kf - e.delta
	u := e.vb.get(db, kb)
	v := u - kf

	// Myers proved there is a df-path from (0,0) to (u,v)
	// and a db-path from (x,y) to (N,M).
	// In the first case, the overall path is the forward path
	// to (u,v) followed by the backward path to (N,M).
	// In the second case, the path is the backward path to (x,y)
	// is followed by the forward path to (x,y) from (0,0).

	// Look for some special cases to avoid computing either of these paths.
	if x == u {
		// "babaab" "cccaba"
		// already patched together
		lcs := e.forwardLcs(df, kf)
		lcs = append(lcs, e.backwardLcs(db, kb)...)

		return lcs.sort()
	}

	// is (u-1,v) or (u,v-1) labelled df-1?
	// if so, that forward df-1-path plus a horizontal or vertical edge
	// is the df-path to (u,v), then plus the db-path to (N,M)
	if u > 0 && ok(df-1, u-1-v) && e.vf.get(df-1, u-1-v) == u-1 {
		//  "aabbab" "cbcabc"
		lcs := e.forwardLcs(df-1, u-1-v)
		lcs = append(lcs, e.backwardLcs(db, kb)...)

		return lcs.sort()
	}

	if v > 0 && ok(df-1, u-(v-1)) && e.vf.get(df-1, u-(v-1)) == u {
		//  "abaabb" "bcacab"
		lcs := e.forwardLcs(df-1, u-(v-1))
		lcs = append(lcs, e.backwardLcs(db, kb)...)

		return lcs.sort()
	}

	// The path can't possibly contribute to the lcs because it
	// is all horizontal or vertical edges
	if u == 0 || v == 0 || x == e.ux || y == e.uy {
		// "abaabb" "abaaaa"
		if u == 0 || v == 0 {
			return e.backwardLcs(db, kb)
		}

		return e.forwardLcs(df, kf)
	}

	// is (x+1,y) or (x,y+1) labelled db-1?
	if x+1 <= e.ux && ok(db-1, x+1-y-e.delta) && e.vb.get(db-1, x+1-y-e.delta) == x+1 {
		// "bababb" "baaabb"
		lcs := e.backwardLcs(db-1, kb+1)
		lcs = append(lcs, e.forwardLcs(df, kf)...)

		return lcs.sort()
	}

	if y+1 <= e.uy && ok(db-1, x-(y+1)-e.delta) && e.vb.get(db-1, x-(y+1)-e.delta) == x {
		// "abbbaa" "cabacc"
		lcs := e.backwardLcs(db-1, kb-1)
		lcs = append(lcs, e.forwardLcs(df, kf)...)

		return lcs.sort()
	}

	// need to compute another path
	// "aabbaa" "aacaba"
	lcs := e.backwardLcs(db, kb)
	oldX, oldY := e.ux, e.uy
	e.ux = u
	e.uy = v
	lcs = append(lcs, e.forward()...)
	e.ux, e.uy = oldX, oldY

	return lcs.sort()
}
